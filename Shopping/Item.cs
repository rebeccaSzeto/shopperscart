﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping
{
    class Item
    {
        public string name { get; set; }
        public decimal price { get; set; }

        public Item()
        {
            this.name = "TBA";
            this.price = 0;
        }

        public Item(string name)
        {
            this.name = name;
        }

        public Item(string name, decimal price)
        {
            this.name = name;
            this.price = price;
        }

        public override string ToString()
        {
            return name + ":  \t$ " + price;
        }

    }
}
