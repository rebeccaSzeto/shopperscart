﻿using System; 
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping
{
    class ShoppingCart : IEquatable<ItemOrder>
    {
        //Stack<string> purchases = new Stack<string>();
        LinkedList<ItemOrder> cart = new LinkedList<ItemOrder>();

        public ShoppingCart()
        {
        }

        public ShoppingCart(ItemOrder cart)
        {
            ItemOrder itemCart = new ItemOrder();
        }

        public void Add(ItemOrder value)
        {
            cart.AddFirst(value);
        }

        public void Remove(string value)
        {
            //run the loop inside a temporary variable so that the value can not be interrupted
            ItemOrder tempOrder = cart.First.Value;
            foreach(ItemOrder k in cart)
            {
                if(k.getItemName().Equals(value))
                {
                    //cart.Remove(k);
                    tempOrder = k;
                }
            }
            cart.Remove(tempOrder);
        }

        public void Find(string value)
        {
            ItemOrder temp = cart.First.Value;
            foreach (ItemOrder k in cart)
            {
                if (k.getItemName().Equals(value))
                {
                    temp = k;
                }
            }            
            cart.Find(temp);
        }

        public void GetCalculateAmount(ItemOrder value)
        {
            value.CalculateAmount();
        }

        public decimal GetDiscount(ItemOrder value)
        {
            //using these two methods calls the inactive one to actually run on the shopping cart.
            return value.CalculateDiscountAmount();
            //value.itemDiscount();
        }

        public bool Equals(ItemOrder itemOrder)
        {
            //what does this do??
            return false;
        }

        public override string ToString()
        {
            string returnString = "";
            int count = 1;
            foreach(ItemOrder k in cart)
            {
                returnString += count + " " + k.ToString() + Environment.NewLine;
                count++;
            }
            return returnString;
        }

    }
}
