﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping
{
    class Program
    {
        private static List<Item> things = new List<Item>();
        //private static List<ItemOrder> purchase = new List<ItemOrder>();
        static void Main(string[] args)
        {
            string[] input = { "Cookies", "Popcorn", "Oranges", "Chocolate", "Pizza" };
            decimal[] cost = { 2.00m, 1.50m, 2.99m, 5.20m, 6.85m };
            AddToPurchases(input, cost);
            DisplayItems();

            ShoppingCart purchase = new ShoppingCart();
            //search for the item you want
            int x = 0;
                do
                {
                    Console.WriteLine("You must make a purchase of 3 items. Please type in the name of your product exactly as its written in the list.");
                    Console.WriteLine("Enter item to buy from the list: ");
                    string search = Console.ReadLine();
                    if (search == "quit")
                    {
                        break;
                    }
                    
                    Item result = things.Find(product => product.name.Equals(search));
                    Console.WriteLine(result.ToString());

                    Console.WriteLine("How many do you want? ");
                    int howMuch = Convert.ToInt32(Console.ReadLine());

                    //take item with item order and quantity and then add it to the shopping cart 
                    purchase.Add(new ItemOrder(result, howMuch));
                    purchase.GetCalculateAmount(new ItemOrder(result, howMuch));
                    //purchase.GetDiscountRate(new ItemOrder());
                    purchase.GetDiscount(new ItemOrder(result, howMuch));
                        
                    x++;
                } while (x < 3);
            Console.WriteLine(purchase.ToString());
            
            //search and remove item or automatically removes the last from the cart
            Console.WriteLine("Enter item to remove: ");
            string search3 = Console.ReadLine();
            purchase.Remove(search3);
            Console.WriteLine(purchase.ToString());

            Console.WriteLine("Enter item to find: ");
            string search4 = Console.ReadLine();
            purchase.Find(search4);
            Console.WriteLine(search4);

            Console.WriteLine(purchase.ToString());

            Console.WriteLine("Thank you for shopping with us.");

            Console.Read();
        }

        private static void AddToPurchases(string[] input, decimal[] cost)
        {
            
            for (int k = 0; k < input.Length; k++)
            {
                things.Add(new Item(input[k], cost[k]));
            }
        }

        public static void DisplayItems()
        {
            Console.WriteLine("This is what you can buy: ");
            foreach (Item m in things)
            {
                Console.WriteLine(m);   
            }
            Console.WriteLine();
        }
        
    }
}
