﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping
{
    class ItemOrder
    {
        //public Item stuff;
        private Item itmStuff;
        private const int MAX_QUANTITY_ITEM_FOR_BULK = 30;
        private const int MIN_QUANTITY_ITEM_FOR_BULK = 20;
        public List<Item> things;
        private int quantity;
        public int Quantity
        {
            get
            {
                return this.quantity;
            }
            set
            {
                if (quantity > 0)
                {
                    this.quantity = value;
                }
                else
                {
                    Console.WriteLine("You must at least enter one item");
                }

            }
        }
        private decimal discountRate;
        public decimal DiscountRate
        {
            get
            {
                return this.discountRate;
            }
            set
            {
                if (quantity > MAX_QUANTITY_ITEM_FOR_BULK)
                { //doesn't read this. Needs to read in the code.
                    this.discountRate = 0.2m;
                } else if (quantity > MIN_QUANTITY_ITEM_FOR_BULK && quantity < MAX_QUANTITY_ITEM_FOR_BULK)
                {
                    this.discountRate = 0.1m;
                }
                
            }
        }
//        private int p;
        

        public ItemOrder()
        {
            this.quantity = 0;
            this.discountRate = 0;
        }

        public ItemOrder(Item stuff)
        {
            this.itmStuff = stuff;
            
        }

        public ItemOrder(Item stuff, int quantity)
        {
            this.itmStuff = stuff;
            this.quantity = quantity;
        }

        public ItemOrder(List<Item> things, int q)
        {
            // TODO: Complete member initialization
            this.things = things;
            this.quantity = q;
        }

        public decimal getItemPrice()
        {
            return itmStuff.price;
        }

        public decimal CompareTo(Object obj)
        {
            Item otherItem = (Item)obj;
            return this.getItemPrice().CompareTo(otherItem.price);
        }

        public string getItemName()
        {
            return itmStuff.name;
        }

        public decimal CalculateAmount()
        {
            return itmStuff.price * quantity;
        }

        public decimal CalculateDiscountAmount()
        {//Calculates the discount by multiplying and subtract from total
            
            if (quantity > MAX_QUANTITY_ITEM_FOR_BULK)
            {
                discountRate = 0.2m;
                
            } else if (quantity > MIN_QUANTITY_ITEM_FOR_BULK && quantity < MAX_QUANTITY_ITEM_FOR_BULK)
            {
                discountRate = 0.1m;
                
            }
            decimal grandTotal = itmStuff.price * quantity;
            decimal discountAmount = discountRate * itmStuff.price * quantity;
            return grandTotal - discountAmount;
        }

        public override string ToString()
        {
            //string output = String.Format("Discount price is: {C2}" + CalculateAmount());
            return itmStuff.ToString() + " " + quantity + "@" + Environment.NewLine + "Before discount: " + CalculateAmount() + " After discount: " + CalculateDiscountAmount();
        }
    }
}
